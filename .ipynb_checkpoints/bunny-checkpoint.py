import time
import os
import pickle as pkl

with open("frames_2.pkl", 'rb') as f:
    frames = pkl.load(f)
i = 0

# Woo! Colors!
colors = ['\033[95m', '\033[94m', '\033[96m', '\033[92m', '\033[93m', '\033[91m']
endColor = '\033[0m'

while True:
    print(colors[i % len(colors)] + frames[i] + endColor)
#     print(frames[i])
    time.sleep(0.2)
    os.system("clear")
    i = (i + 1) % len(frames)
